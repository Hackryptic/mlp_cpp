/*
 * File Name: MLPAPI.cpp
 * Written On Jan 24, 2019
 * Written by Jeremy Sun <shggreen@hotmail.com>
 */

#include "MLPAPI.h"
#include <iostream>
#include <time.h>
#include <math.h>
 

Layer::Layer(int input_size, int node_num, BackPropagator* prop_type)
{
	this->input_size = input_size;
	this->node_num = node_num;
	this->prop_type = prop_type;
	layer_output.resize(node_num, 0.f);
	gradient.resize(input_size * node_num, 0.f);
	weight.resize(input_size * node_num, 0.f);

	srand(time(NULL));

	for (int i = 0; i < weight.size(); i++)
		weight[i] = (rand() % 11) * 0.1f;
}


FloatVector Layer::propagateForward(FloatVector input)
{
	layer_input = input;
	float net = 0.f;

	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{
			net += layer_input[input_index]
				* weight[node_index * input_size + input_index];
		}

		layer_output[node_index] = sigmoid(net);
	}

	return layer_output;
}

FloatVector Layer::propagateBack(FloatVector medium)
{
	FloatVector delta;
	FloatVector temp_gradient;
	int gradient_size = gradient.size();

	temp_gradient = prop_type->computeGradient(layer_input, layer_output, medium);

	for (int gradient_index = 0; gradient_index < gradient_size; gradient_index++)
	{
		gradient[gradient_index] += temp_gradient[gradient_index];
	}

	delta = prop_type->computeDelta(weight, layer_output, medium);

	return delta;
}

void Layer::updateWeight(float learning_rate)
{
	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{
			weight[node_index * input_size + input_index] += (-learning_rate) * gradient[node_index * input_size + input_index];
			gradient[node_index * input_size + input_index] = 0.f;
		}
	}
}

float Layer::sigmoid(float net)
{
	float result = 1 / (1 + exp(-net));

	return result;
}

FloatVector BackPropOutput::computeGradient(FloatVector layer_input, FloatVector layer_output, FloatVector desired_output)
{
	int input_size = layer_input.size();
	int node_num = layer_output.size();
	FloatVector gradient(input_size * node_num, 0);
	
	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{
			gradient[node_index * input_size + input_index] = (layer_output[node_index] - desired_output[node_index]) / node_num
				* layer_output[node_index] * (1 - layer_output[node_index])
				* layer_input[input_index];
		}

	} 

	return gradient;
}

FloatVector BackPropOutput::computeDelta(FloatVector weight, FloatVector layer_output, FloatVector desired_output)
{
	int node_num = layer_output.size();
	int input_size = weight.size() / node_num;

	FloatVector prev_delta(input_size, 0);

	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{		
			prev_delta[input_index] += (layer_output[node_index] - desired_output[node_index]) / node_num
				* layer_output[node_index] * (1 - layer_output[node_index])
				* weight[node_index * input_size + input_index];
		}

	}

	return prev_delta;
}

FloatVector BackPropHidden::computeGradient(FloatVector layer_input, FloatVector layer_output, FloatVector delta)
{
	int input_size = layer_input.size();
	int node_num = layer_output.size();
	FloatVector gradient(input_size * node_num, 0);

	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{
			gradient[node_index * input_size + input_index] = delta[node_index] * layer_output[node_index] * (1 - layer_output[node_index]) * layer_input[input_index];
		}
	}

	return gradient;
}

FloatVector BackPropHidden::computeDelta(FloatVector weight, FloatVector layer_output, FloatVector delta)
{
	int node_num = layer_output.size();
	int input_size = weight.size() / node_num;
	FloatVector prev_delta(input_size, 0);

	for (int node_index = 0; node_index < node_num; node_index++)
	{
		for (int input_index = 0; input_index < input_size; input_index++)
		{
			prev_delta[input_index] += delta[node_index] * layer_output[node_index] * (1 - layer_output[node_index]) * weight[node_index * node_num + input_index];
		}
	}

	return prev_delta;
}

MLPNetwork::MLPNetwork(int input_size, std::vector<int> layer_nodes, float learning_rate, int max_epoch, int batch_size, float max_error)
{
	this->layer_num = layer_nodes.size();
	this->learning_rate = learning_rate;
	this->batch_size = batch_size;
	this->input_size = input_size;
	this->max_epoch = max_epoch;
	this->max_error = max_error;
	
	layer_list.resize(layer_num);

	layer_list[0] = std::unique_ptr<Layer>(new Layer(input_size, layer_nodes[0], new BackPropHidden()));


	for (int layer_index = 1; layer_index < layer_num - 1; layer_index++)
	{
		layer_list[layer_index] = std::unique_ptr<Layer>(new Layer(layer_nodes[layer_index-1], layer_nodes[layer_index], new BackPropHidden()));
	}

	layer_list[layer_num-1] = std::unique_ptr<Layer>(new Layer(layer_nodes[layer_num-2], layer_nodes[layer_num-1], new BackPropOutput()));
}

FloatVector MLPNetwork::propagateForward(FloatVector input)
{
	FloatVector cur_output = input;

	for (int layer_index = 0; layer_index < layer_num; layer_index++)
	{
		cur_output = layer_list[layer_index]->propagateForward(cur_output);
	}

	return cur_output;
}

void MLPNetwork::propagateBack(FloatVector desired_output)
{
	FloatVector cur_medium = desired_output;

	for (int layer_index = layer_num - 1; layer_index >= 0; layer_index--)
	{
		cur_medium = layer_list[layer_index]->propagateBack(cur_medium);
	}
}

void MLPNetwork::trainNetwork(FloatVector2D input_list, FloatVector2D desired_output_list)
{
	int data_num = input_list.size();
	int epoch = 0;
	int data_index = 0;
	float error_rate;

	while (epoch < max_epoch)
	{
		data_index = epoch % data_num;
		propagateForward(input_list[data_index]);
		propagateBack(desired_output_list[data_index]);

		if (epoch % batch_size == batch_size-1)
		{
			updateWeight();
			error_rate = getErrorRate(input_list, desired_output_list);

			std::cout << "epoch " << epoch << " error rate: " << error_rate << std::endl;

			if (error_rate < max_error)
				break;
		}

		epoch++;
	}

	printOutput(input_list, desired_output_list);
}

void MLPNetwork::updateWeight()
{
	for (int layer_index = 0; layer_index < layer_num; layer_index++)
	{
		layer_list[layer_index]->updateWeight(learning_rate);
	}
}

float MLPNetwork::getErrorRate(FloatVector2D input_list, FloatVector2D desired_output_list)
{
	int data_num = input_list.size();
	int output_size = desired_output_list[0].size();
	float mse = 0.f;
	float total_error = 0.f;
	FloatVector cur_output;

	for (int data_index = 0; data_index < data_num; data_index++)
	{
		cur_output = propagateForward(input_list[data_index]);

		for (int att_index = 0; att_index < output_size; att_index++)
		{
			mse += (cur_output[att_index] - desired_output_list[data_index][att_index])
				* (cur_output[att_index] - desired_output_list[data_index][att_index]);
		}

		mse = (mse / 2) / output_size;

		total_error += mse;
	}

	total_error /= data_num;

	return total_error;
}

void MLPNetwork::printOutput(FloatVector2D input_list, FloatVector2D desired_output_list)
{
	int data_num = input_list.size();
	int output_size = desired_output_list[0].size();
	FloatVector cur_output;
	
	for (int data_index = 0; data_index < data_num; data_index++)
	{
		std::cout << "[";

		for (int att_index = 0; att_index < input_size; att_index++)
		{
			std::cout << input_list[data_index][att_index] << ", ";
		}

		std::cout << "] = [";

		cur_output = propagateForward(input_list[data_index]);

		for (int att_index = 0; att_index < output_size; att_index++)
		{
			std::cout << cur_output[att_index] << ", ";
		}

		std::cout << "]" << std::endl;
	}
}