/* 
 * File Name: MLPAPI.h
 * Written On Jan 24, 2019 
 * Written by Jeremy Sun <shggreen@hotmail.com>
 */
#include <vector>
#include <memory>

#ifndef MLPAPI
#define MLPAPI

class Layer;

typedef std::vector<float> FloatVector;
typedef std::vector<FloatVector> FloatVector2D;
typedef std::vector<std::unique_ptr<Layer>> LayerList;


class BackPropagator
{
public:
	virtual FloatVector computeGradient(FloatVector, FloatVector, FloatVector) = 0;
	virtual FloatVector computeDelta(FloatVector, FloatVector, FloatVector) = 0;
};

class BackPropHidden : public BackPropagator
{

public:
	FloatVector computeGradient(FloatVector, FloatVector, FloatVector);
	FloatVector computeDelta(FloatVector, FloatVector, FloatVector);
};

class BackPropOutput : public BackPropagator
{
public:
	FloatVector computeGradient(FloatVector, FloatVector, FloatVector);
	FloatVector computeDelta(FloatVector, FloatVector, FloatVector);
};

class Layer
{
	private:
		FloatVector weight;
		FloatVector layer_input;
		FloatVector layer_output;
		FloatVector gradient;
		float bias;
		int input_size;
		int node_num;
		BackPropagator* prop_type;

	public:
		Layer(int,int, BackPropagator*);
		FloatVector propagateForward(FloatVector);
		FloatVector propagateBack(FloatVector);
		void updateWeight(float);
		float sigmoid(float);
};


class MLPNetwork
{
	private:		
		LayerList layer_list;
		FloatVector final_output;
		FloatVector network_input;
		FloatVector network_output;
		FloatVector desired_output;
		int layer_num;
		float learning_rate;
		int batch_size;
		int max_epoch;
		int input_size;
		float max_error;

	public:
		MLPNetwork(int input_size, std::vector<int>, float, int, int, float);
		void trainNetwork(FloatVector2D, FloatVector2D);
		FloatVector propagateForward(FloatVector);
		void propagateBack(FloatVector);
		void updateWeight();
		float getErrorRate(FloatVector2D, FloatVector2D);
		void printOutput(FloatVector2D, FloatVector2D);
};


#endif
