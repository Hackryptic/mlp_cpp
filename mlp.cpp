/*
 * File Name: mlp.cpp
 * Written On Jan 24, 2019
 * Written Jeremy Sun <shggreen@hotmail.com>
 */
#include "MLPAPI.h"
using namespace std;
#define MAX_EPOCH 1000000
#define MAX_ERROR 0.01
#define LAYER_NUMBER 2
#define LEARNING_RATE 0.1
#define BATCH_SIZE 1


int main()
{
	FloatVector2D input_data = { {0.f,0.f}, {0.f,1.f}, {1.f,0.f}, {1.f,1.f} };
	FloatVector2D desired_data = { {0.f}, {1.f}, {1.f}, {0.f} };
	vector<int> layer_info = { 2, 1 };
	

	MLPNetwork mlp_network(input_data[0].size(), layer_info, LEARNING_RATE, MAX_EPOCH, BATCH_SIZE, MAX_ERROR);
	mlp_network.trainNetwork(input_data, desired_data);
}